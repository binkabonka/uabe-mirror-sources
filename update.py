import json
import os

name = "UABEM"
email = "UABEM@noreply.com"

version = list(json.load(open("releases.json")).keys())[-1]
print(f"latest uabe version: {version}")

for person in ["AUTHOR", "COMMITTER"]:
    os.environ[f"GIT_{person}_NAME"] = name
    os.environ[f"GIT_{person}_EMAIL"] = email
    
for cmd in [
    f"git config user.name {name}",
    f"git config user.email {email}",
    f"git add .",
    f"git commit -m {version}",
    f"git push -u origin --all"
]:
    print(f"> {cmd}")
    os.system(cmd)
